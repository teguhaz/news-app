package id.taz.news.ext

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

const val REQUEST_CODE_PERMISSIONS = 11101
const val CAMERA_PERMISSION = Manifest.permission.CAMERA
const val ACCESS_FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION
const val ACCESS_COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION
const val READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE
const val WRITE_INTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE

fun AppCompatActivity.requestPermissions() {
    val permissions = mutableListOf<String>()
    if (!checkSinglePermission(this, CAMERA_PERMISSION)) {
        permissions.add(CAMERA_PERMISSION)
    }
    if (!checkSinglePermission(this, ACCESS_FINE_LOCATION)) {
        permissions.add(ACCESS_FINE_LOCATION)
    }
    if (!checkSinglePermission(this, ACCESS_COARSE_LOCATION)) {
        permissions.add(ACCESS_COARSE_LOCATION)
    }
    if (!checkSinglePermission(this, READ_EXTERNAL_STORAGE)) {
        permissions.add(READ_EXTERNAL_STORAGE)
    }
    if (!checkSinglePermission(this, WRITE_INTERNAL_STORAGE)) {
        permissions.add(WRITE_INTERNAL_STORAGE)
    }
    if (permissions.isNotEmpty()) {
        ActivityCompat.requestPermissions(
            this, permissions.toTypedArray(), REQUEST_CODE_PERMISSIONS
        )
    }
}

private fun checkSinglePermission(context: Context, permission: String): Boolean {
    return ContextCompat.checkSelfPermission(
        context,
        permission
    ) == PackageManager.PERMISSION_GRANTED
}