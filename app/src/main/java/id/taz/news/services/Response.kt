package id.taz.news.services

import androidx.annotation.Keep

import com.google.gson.annotations.SerializedName


class Response<T> (
    @SerializedName("success") val success: Boolean,
    @SerializedName("code") val code: Int?,
    @SerializedName("message") val message: String,
    @SerializedName("data") val data: T
)

@Keep
data class SuccessResponse(
    @SerializedName("success") val success: Boolean,
    @SerializedName("code") val code: Int?,
    @SerializedName("message") val message: String
)

@Keep
data class ErrorResponse(
    @SerializedName("status") val status: Boolean,
    @SerializedName("message") val message: String
)