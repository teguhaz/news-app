package id.taz.news.services.rest

import id.taz.news.services.Response
import id.taz.news.services.entity.ResponseNews
import retrofit2.http.GET
import retrofit2.http.Query

interface RestNew {

    @GET("everything?q=apple")
    suspend fun getNews(
        @Query("from") from: String, // yyyy-MM-dd
        @Query("sortBy") sortBy: String, // popularity
        @Query("apiKey") apiKey: String
    ): ResponseNews
}