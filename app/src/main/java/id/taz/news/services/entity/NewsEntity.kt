package id.taz.news.services.entity

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Keep
data class ResponseNews(
    @SerializedName("articles") val articles: List<ResponseNewItem>,
)

@Keep
data class ResponseNewItem(
    @SerializedName("source") val source: ResponseSource,
    @SerializedName("author") val author: String,
    @SerializedName("title") val title: String,
    @SerializedName("description") val description: String,
    @SerializedName("url") val url: String,
    @SerializedName("urlToImage") val urlToImage: String,
    @SerializedName("publishedAt") val publishedAt: String,
    @SerializedName("content") val content: String,
) : Serializable

@Keep
data class ResponseSource(
    @SerializedName("id") val id: String?,
    @SerializedName("name") val name: String
) : Serializable