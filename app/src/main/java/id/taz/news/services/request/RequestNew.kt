package id.taz.news.services.request

import androidx.annotation.Keep
import retrofit2.http.Query

@Keep
data class RequestNews(
    @Query("from") val from: String, // yyyy-MM-dd
    @Query("sortBy") val sortBy: String, // popularity
    @Query("apiKey") val apiKey: String
)