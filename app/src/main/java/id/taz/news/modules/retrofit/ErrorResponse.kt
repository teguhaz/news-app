package id.taz.news.modules.retrofit

import androidx.annotation.Keep

import com.google.gson.annotations.SerializedName

@Keep
data class ErrorResponse(
    @SerializedName("status") val status: Boolean,
    @SerializedName("code") val code: Int,
    @SerializedName("message") val message: String
)
