package id.taz.news.modules.retrofit

import android.util.Log
import com.google.gson.Gson
import id.taz.news.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import okhttp3.internal.http2.ConnectionShutdownException
import okio.IOException
import retrofit2.HttpException
import retrofit2.Retrofit
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.security.cert.CertificateException

object ResponseInterceptor : Interceptor  {

    var retrofit: Retrofit? = null

    var token: String? = null

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val headers = request.headers.values("@")
        if (headers.contains("Auth")) {
            val builder = request.newBuilder()
                .header("Authorization", "$token")
            request = builder.removeHeader("@").build()
        }
        val response = try {
            chain.proceed(request)
        } catch (error: Exception) {
            val message = when (error) {
                is SocketTimeoutException -> "Koneksi timeout"
                is UnknownHostException -> "Tidak ada koneksi, mohon periksa koneksi internet anda"
                is ConnectionShutdownException -> "Tidak ada koneksi, mohon periksa koneksi internet anda"
                is HttpException -> "Ada kesalahan, silakan coba lagi"
                is CertificateException -> "Membutuhkan sambungan koneksi terpercaya"
                is java.io.IOException -> "Ada kesalahan, silakan coba lagi"
                else -> "Ada kesalahan, silakan coba lagi"
            }
            throw java.io.IOException(message)
        }

        return response

//        if(response.code == 200){
//            Log.d("error code", "code: ${Gson().toJson(response.body?.contentType())}")
//            return response
//        } else if(response.code in 400 .. 401){
//            val message = "Akun anda telah login diperangkat lain, silakan login kembali"
//            val errorResponse = ErrorResponse(false, response.code, message)
//            return response.newBuilder()
//                .code(200)
//                .body(ResponseBody.create(response.body?.contentType(), Gson().toJson(errorResponse)))
//                .build()
//        } else {
//            val message = "Ada kesalahan, silakan coba lagi"
//            val errorResponse = ErrorResponse(false, response.code, message)
//            return response.newBuilder()
//                .code(200)
//                .body(ResponseBody.create(response.body?.contentType(), Gson().toJson(errorResponse)))
//                .build()
//        }
    }
}