package id.taz.news.common.storage

import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Preferences @Inject constructor(private val prefs: SharedPreferences) {

    var userid: String by PreferenceData(prefs, "userid", "")
    var token: String by PreferenceData(prefs, "token", "")
    var status: String by PreferenceData(prefs, "status", "")

    fun clear() {
        prefs.edit().clear().apply()
    }
}