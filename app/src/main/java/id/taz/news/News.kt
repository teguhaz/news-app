package id.taz.news

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class News : Application()