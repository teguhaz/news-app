package id.taz.news.ui.main.home

import android.os.Bundle
import com.bumptech.glide.Glide
import id.taz.news.databinding.NewsDetailActivityBinding
import id.taz.news.modules.base.BaseActivity
import id.taz.news.services.entity.ResponseNewItem

class NewsDetailActivity : BaseActivity() {

    companion object {
        val TAG = NewsDetailActivity::class.java.simpleName
        val NEWS = "news"
    }

    private lateinit var mBinding: NewsDetailActivityBinding

    private var mResponseNewItem: ResponseNewItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = NewsDetailActivityBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mResponseNewItem = (intent?.extras?.getSerializable(NEWS)) as ResponseNewItem?

        initToolbar()
        initView()
    }

    private fun initToolbar(){
        mBinding.toolbar.tvTitle.text = "News Detail"
        mBinding.toolbar.back.setOnClickListener { onBackPressed() }
        mResponseNewItem?.let {
            mBinding.toolbar.tvTitle.text = it.title
        }
    }

    private fun initView() {
        mResponseNewItem?.let { data ->
            with(mBinding){
//                tvDate.text = data.publishedAt.trim().replace("\\s", "").replace("\\n", "")
                tvNewsTitle.text = data.title
//                    .trim().replace("\\s", "").replace("\\n", "")
                tvContent.text = data.content.trim().replace("\\s", "").replace("\\n", "")
                tvSourceName.text = data.source.name.trim().replace("\\s", "").replace("\\n", "")

                Glide.with(this@NewsDetailActivity)
                    .load(data.urlToImage)
                    .thumbnail(0.1f)
                    .into(ivImage)
            }
        }
    }
}