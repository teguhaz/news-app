package id.taz.news.ui

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import id.taz.news.R
import java.text.SimpleDateFormat
import java.util.*

class Popup {

    companion object {

        fun PopupError(context: Context, msg: String, onClickOK: () -> Unit) {
            val li = LayoutInflater.from(context)
            val promptsView: View = li.inflate(R.layout.popup_gagal, null)

            val alertDialogBuilder = AlertDialog.Builder(context)
            alertDialogBuilder.setView(promptsView)
            val alertDialog = alertDialogBuilder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setCancelable(false)

            val message = promptsView.findViewById<TextView>(R.id.message)
            message.setText(msg)

            val btnok = promptsView.findViewById<Button>(R.id.btnok)
            btnok.setOnClickListener {
                alertDialog.dismiss()
                onClickOK()
            }

            alertDialog.show()
            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

        fun PopupError(context: Context, msg: String) {
            val li = LayoutInflater.from(context)
            val promptsView: View = li.inflate(R.layout.popup_gagal, null)

            val alertDialogBuilder = AlertDialog.Builder(context)
            alertDialogBuilder.setView(promptsView)
            val alertDialog = alertDialogBuilder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setCancelable(false)

            val message = promptsView.findViewById<TextView>(R.id.message)
            message.setText(msg)

            val btnok = promptsView.findViewById<Button>(R.id.btnok)
            btnok.setOnClickListener { alertDialog.dismiss() }

            alertDialog.show()
            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

        fun PopupNotConnect(context: Context, msg: String) {
            val li = LayoutInflater.from(context)
            val promptsView: View = li.inflate(R.layout.popup_gagal, null)

            val alertDialogBuilder = AlertDialog.Builder(context)
            alertDialogBuilder.setView(promptsView)
            val alertDialog = alertDialogBuilder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setCancelable(false)

            val message = promptsView.findViewById<TextView>(R.id.message)
            message.setText(msg)

            val btnok = promptsView.findViewById<Button>(R.id.btnok)
            btnok.setOnClickListener { alertDialog.dismiss() }
            btnok.setText("OK")

            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

        fun PopupSuccess(context: Context, msg: String) {
            val li = LayoutInflater.from(context)
            val promptsView: View = li.inflate(R.layout.popup_success, null)

            val alertDialogBuilder = AlertDialog.Builder(context)
            alertDialogBuilder.setView(promptsView)
            val alertDialog = alertDialogBuilder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setCancelable(false)

            val message = promptsView.findViewById<TextView>(R.id.message)
            message.setText(msg)

            val btnok = promptsView.findViewById<Button>(R.id.btnok)
            btnok.setOnClickListener { alertDialog.dismiss() }

            alertDialog.show()
            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

        fun PopupSuccess(context: Context, msg: String, onClickOK: () -> Unit) {
            val li = LayoutInflater.from(context)
            val promptsView: View = li.inflate(R.layout.popup_success, null)

            val alertDialogBuilder = AlertDialog.Builder(context)
            alertDialogBuilder.setView(promptsView)
            val alertDialog = alertDialogBuilder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setCancelable(false)

            val message = promptsView.findViewById<TextView>(R.id.message)
            message.setText(msg)

            val btnok = promptsView.findViewById<Button>(R.id.btnok)
            btnok.setOnClickListener {
                onClickOK()
                alertDialog.dismiss()
            }

            alertDialog.show()
            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

        fun PopupConfirm(context: Context, msg: String, onClickConfirm: (isOK: Boolean) -> Unit) {
            val li = LayoutInflater.from(context)
            val promptsView: View = li.inflate(R.layout.popup_konfirmasi, null)

            val alertDialogBuilder = AlertDialog.Builder(context)
            alertDialogBuilder.setView(promptsView)
            val alertDialog = alertDialogBuilder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setCancelable(false)

            val message = promptsView.findViewById<TextView>(R.id.message)
            message.setText(msg)

            val btnok = promptsView.findViewById<Button>(R.id.btnYes)
            btnok.setOnClickListener {
                onClickConfirm(true)
                alertDialog.dismiss()
            }

            val btnNo = promptsView.findViewById<Button>(R.id.btnNo)
            btnNo.setOnClickListener {
                onClickConfirm(false)
                alertDialog.dismiss()
            }

            alertDialog.show()
            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }


        fun DatePicker(context: Context, onSelected: (result: Date) -> Unit) {
            val c = Calendar.getInstance()
            val year = c[Calendar.YEAR]
            val month = c[Calendar.MONTH]
            val day = c[Calendar.DAY_OF_MONTH]

            val dialog = DatePickerDialog(context, { _, year, month, dayOfMonth ->
                val Date = year.toString() + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%02d", dayOfMonth)
                val fmt = SimpleDateFormat("yyyy-MM-dd")
                val result = fmt.parse(Date)
                onSelected(result)
            }, year, month, day)
            dialog.setCanceledOnTouchOutside(false)
            dialog.show()
        }

        fun DatePicker(context: Context, dateInput: Date, onSelected: (result: Date) -> Unit) {
            val c = Calendar.getInstance()
            c.time = dateInput
            val year = c[Calendar.YEAR]
            val month = c[Calendar.MONTH]
            val day = c[Calendar.DAY_OF_MONTH]
            val dialog = DatePickerDialog(context, { _, year, month, dayOfMonth ->
                val Date = year.toString() + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%02d", dayOfMonth)
                val fmt = SimpleDateFormat("yyyy-MM-dd")
                val result = fmt.parse(Date)
                onSelected(result)
            }, year, month, day)
            dialog.setCanceledOnTouchOutside(false)
            dialog.show()
        }
    }
}