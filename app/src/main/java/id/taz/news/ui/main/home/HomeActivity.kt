package id.taz.news.ui.main.home

import android.content.Intent
import android.os.Bundle
import android.view.View.GONE
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import id.taz.news.common.UiState
import id.taz.news.databinding.HomeActivityBinding
import id.taz.news.ext.observe
import id.taz.news.ext.toast
import id.taz.news.modules.base.BaseActivity
import id.taz.news.ui.Popup
import id.taz.news.viewmodel.NewsViewModel
import java.util.*

class HomeActivity : BaseActivity() {

    companion object {
        val TAG = HomeActivity::class.java.simpleName
    }

    private lateinit var mBinding: HomeActivityBinding
    private val newsViewModel by viewModels<NewsViewModel>()

    private val newsAdapter by lazy {
        NewsAdapter {
            val intent = Intent(this, NewsDetailActivity::class.java)
            intent.putExtra(NewsDetailActivity.NEWS, it)
            startActivity(intent)
        }
    }

    // --- untuk back to close app ---
    private var mRecentlyBackPressed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = HomeActivityBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        initToolbar()
        initView()
        handleState()
    }

    private fun initToolbar(){
        mBinding.toolbar.tvTitle.text = "Teguh News"
        mBinding.toolbar.back.visibility = GONE
    }

    private fun initView() {
        mBinding.swipeRefreshLayout.setOnRefreshListener {
            newsViewModel.getNewa(date = Date(), sortBy = "popularity")
            mBinding.swipeRefreshLayout.isRefreshing = false
        }

        mBinding.recyclerView.layoutManager = GridLayoutManager(this, 2)
        mBinding.recyclerView.adapter = newsAdapter

        newsViewModel.getNewa(date = Date(), sortBy = "popularity")
    }

    private fun handleState() {
        observe(newsViewModel.responseNews){
            if(it != null) newsAdapter.items = it.articles
        }

        observe(newsViewModel.loadState){
            when (it){
                UiState.Loading -> mBinding.progress.progress.isVisible = true
                UiState.Success -> {
                    mBinding.progress.progress.isVisible = false
                }
                is UiState.Error -> {
                    mBinding.progress.progress.isVisible = false
                    Popup.PopupError(this, it.message)
                }
                is UiState.Failed -> {
                    mBinding.progress.progress.isVisible = false
                    Popup.PopupError(this, it.message)
                }
            }
        }
    }

    override fun onBackPressed() {
        if (mRecentlyBackPressed) {
            ActivityCompat.finishAffinity(this)
        } else {
            toast("Tekan sekali lagi untuk keluar")
            mRecentlyBackPressed = true
        }
    }
}