package id.taz.news.ui.main.home

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.taz.news.R
import id.taz.news.databinding.NewsItemBinding
import id.taz.news.ext.inflate
import id.taz.news.services.entity.ResponseNewItem

class NewsAdapter(private val onClick:(ResponseNewItem) -> Unit)
    : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    var items : List<ResponseNewItem> = emptyList()
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.news_item))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        (holder as? ViewHolder)?.bind(items[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        private var mBinding = NewsItemBinding.bind(itemView)

        fun bind(data: ResponseNewItem){
            with(mBinding){
                tvDate.text = data.publishedAt.trim().replace("\\s", "").replace("\\n", "")
                tvTitle.text = data.title.trim().replace("\\s", "").replace("\\n", "")
                tvContent.text = data.content.trim().replace("\\s", "").replace("\\n", "")
                tvSourceName.text = data.source.name.trim().replace("\\s", "").replace("\\n", "")

                Glide.with(itemView.context)
                    .load(data.urlToImage)
                    .thumbnail(0.1f)
                    .into(ivImage)

                cardView.setOnClickListener {
                    onClick(data)
                }
            }
        }
    }
}