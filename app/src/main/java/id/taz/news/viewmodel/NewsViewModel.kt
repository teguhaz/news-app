package id.taz.news.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import id.taz.news.common.ActionLiveData
import id.taz.news.common.UiState
import id.taz.news.common.storage.Preferences
import id.taz.news.services.rest.RestNew
import dagger.hilt.android.lifecycle.HiltViewModel
import id.taz.news.BuildConfig
import id.taz.news.ext.errorMesssage
import id.taz.news.ext.toYYYYMMDD
import id.taz.news.services.entity.*
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class NewsViewModel @Inject constructor(
    private val preferences: Preferences,
    private val restNew: RestNew
) : ViewModel() {

    val loadState = ActionLiveData<UiState>()
    val sendState = ActionLiveData<UiState>()

    val responseNews = ActionLiveData<ResponseNews>()

    fun getNewa(date: Date?, sortBy: String) {
        viewModelScope.launch {
            loadState.sendAction(UiState.Loading)
            try {
                val date = date ?: Date()
                val response = restNew.getNews(
                    from = date.toYYYYMMDD(),
                    sortBy = sortBy,
                    apiKey = BuildConfig.Apikey
                )
                Log.d(this::class.java.simpleName, "response : ${Gson().toJson(response)}")
                responseNews.value = response
                loadState.sendAction(UiState.Success)
//                if (response.success) {
//                } else {
//                    if(response.code != null){
//                        loadState.sendAction(UiState.Failed(response.code, response.message))
//                    } else {
//                        loadState.sendAction(UiState.Error(response.message))
//                    }
//                }
            } catch (e: Exception){
                loadState.sendAction(UiState.Error(e.errorMesssage))
            }
        }
    }
}