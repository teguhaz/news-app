package id.taz.news.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import id.taz.news.common.storage.Preferences
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RepoViewModel @Inject constructor(
    private val preferences: Preferences,
    val application: Application,
) : ViewModel() {

    fun getUserId() = preferences.userid
    fun logout() = preferences.clear()

}